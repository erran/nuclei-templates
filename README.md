# Nuclei Templates

My personal sandbox for testing attack "simulation" through nuclei.

Check out the [pipelines](https://gitlab.com/erran/nuclei-templates/-/pipelines) to see results from running [nuclei-analyzer][]

See [nuclei-analyzer][] for the nuclei DAST analyzer project.

## Pre-requisites

Install Docker Desktop and Docker Compose.

## Usage

In one Terminal pane start some known exploitable services:

```
cd kev/
docker compose up
```

After successful startup run nuclei from the root of this repository.

```
nuclei -v -vv -u https://127.0.0.1:8443 -t templates/dotcms.yaml -headless -store-resp -json
```

Use `jq` or some magic to convert  the nuclei results into the DAST report schema.

Create a gitlab pipeline with all of the above.

[nuclei-analyzer]: https://gitlab.com/gitlab-org/incubation-engineering/breach-and-attack-simulation/dast/nuclei-analyzer/
